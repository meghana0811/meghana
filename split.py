#  Write a program to split the equal weighted groups from the below mentioned numbers?
# Ex: Total Groups - 2
# 	Input list - [1,7,8,2]
# 	Output - [[1,8], [7,2]]

# Ex: Total Groups - 3
# 	Input list - [1,7,8,2,3,2,1,6,3]
# 	Output - [[1,7,3], [8,2,1], [2,6,3]]

# Ex: Total Groups - 4
# 	Input list - [1,7,2,3,1,8,2,3,2,1,6,3]
# 	Output - [[1,7,2], [2,8], [2,3,3,1], [6,3]]

def SplitPoint(arr, n): 
    leftSum = 0
    for i in range(0,n):
        leftSum += arr[i] 
        rightSum = 0
        for j in range(i+1, n): 
            rightSum += arr[j] 
        if (leftSum == rightSum): 
            return i+1
    return -1
def printTwoParts(arr, n): 
    split = SplitPoint(arr, n) 
    if (split == -1 or split == n ): 
        print ("Not Possible") 
        return
    for i in range(0, n): 
        if(split == i): 
            print ("") 
    print (str(arr[i]) + ' ',end = '') 
arr =  [1,7,8,2]
n = len(arr) 
printTwoParts(arr, n)   

 
 
