# Write a program to print the total size of files in a given directory and arrange them in 
# ascending order (based on file size and directories should be printed at the bottom)?

echo " Directory "
listingFilesInDirectory(){
    list=`ls $1`
    echo '-------------------------'
    IFS=$'\n'
    for line in $list
    do
        file_permission=`echo $line | awk -F' ' '{print $1}'`
        file_name=`echo $line | awk -F' ' '{print $9}'`
        if (du -h $list='d')
        then 
            echo "${file_name} directory"
            directory_path=`echo "${1}/${file_name}"`
            echo $directory_path
            listingFilesInDirectory $directory_path
            echo $file_name
            sort -g $file_name
        fi   
    done
}
input_path=$1
listingFilesInDirectory $input_path   
