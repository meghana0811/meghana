import socket
from fabonacci import Fabonacci
s = socket.socket()	
port = 12349
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

s.bind(('', port))
s.listen(5)

fabonacci.read_json()

while True:
    c, addr = s.accept()	 
    c.send(str(Fabonacci.print_program()).encode())
    c.close()
